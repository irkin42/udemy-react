const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/test');

const carSchema = mongoose.Schema({
    brand: String,
    model: String,
    year: Number,
    avail: Boolean,
});

const Car = mongoose.model('Car', carSchema);

// const addCar = new Car({
//     brand: 'Ford',
//     model: 'Focus',
//     year: 2017,
//     avail: true,
// })

// addCar.save((err,doc)=>{
//     if(err) return console.log(err);
//     console.log(doc)
// })

// Car.findById('5af8d4457ede6101dc0206dc',  (err,doc)=>{
//     if(err) return console.log(err)
//     console.log(doc)
// })

// Car.update(
//     {_id: '5af8d43df868fb2dfc67a038'}, 
//     {$set: {brand: 'Nissan'}},
//     (err,doc)=>{
//             if(err) return console.log(err)
//     console.log(doc)

//     }
// )
// Car.findByIdAndUpdate(
//     '5af8d43df868fb2dfc67a038', 
//     {$set: {brand: 'Ferrari'}},
//     {new: false},
//     (err,doc)=>{
//             if(err) return console.log(err)
//     console.log(doc)

//     }
// )
Car.findById(
    '5af8d43df868fb2dfc67a038', 
    (err,car)=>{
    if(err) return console.log(err)
    car.set({
        brand: 'Porche'
    })
    car.save((err,doc)=>{
        if(err) return console.log(err)
        console.log(doc)
    })

    }
)