import React, {PureComponent} from 'react';

class Life extends PureComponent{
	// 1 get default props
	// 2 set default state
	state = {
		title: 'Life cycles'
	}
	// 5 after jxs - once
	// componentDidMount(){
	// 	console.log('5 after render');
	// 	document.querySelector('h3').style.color = 'red';

	// }

	// // 3 before render - once
	// componentWillMount(){
	// 	console.log('3 before render');
	// }

	// componentWillUpdate(){
	// 	console.log('before update')
	// }

	// componentDidUpdate(){
	// 	console.log('after update')
	// }

	// shouldComponentUpdate(nextProps,nextState){
	// 	// console.log(this.state.title);
	// 	// console.log(nextState.title);

	// 	if (nextState.title === this.state.title) {
	// 		return false
	// 	}
	// 	return true;
	// }

	// componentWillReceiveProps(){
	// 	console.log('before receive props');
	// }

	// componentWillUnmount(){
	// 	console.log("unmount")
	// }
	// 4 render jxs
	render(){
		console.log('render')
		return (
			<div onClick={()=> this.setState({title:'something else'})}>
				<h3>{this.state.title}</h3>
			click to change</div>
	)

	}
}
export default Life;