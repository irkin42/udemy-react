import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, NavLink, NavNavLink, Switch} from 'react-router-dom';
// interacts with url & go and execute what was passed / COMPONENTS

import Home from './components/home';
import Posts from './components/posts';
import PostItem from './components/post_item';
import Profile from './components/profiles';
import Life from './components/lifecycles';
import User from './components/user';
import Conditional from './components/conditional';

const App = () => {
    return (
        <BrowserRouter>
            <div>
                <header>
                    <NavLink to="/">Home</NavLink><br/>
                    <NavLink
                        to="/posts"
                        activeStyle={{
                        color: 'red'
                    }}
                        activeClassName="selected">Posts
                    </NavLink><br/>
                    <NavLink
                        to={{
                        pathname: '/profiles',
                        hash: '#irini',
                        search: '?profile=true'
                    }}>Profile</NavLink><br/>
                    <NavLink to="/life">Life</NavLink><br/>
                    <NavLink to="/conditional">Conditional</NavLink><br/>
                    <NavLink to="/user">User</NavLink>
                    <hr/>
                </header>
                <Switch>
                    <Route path="/posts/:id/:username" component={PostItem}/>
                    <Route path="/posts" component={Posts}/>
                    <Route path="/life" component={Life}/>
                    <Route path="/conditional" component={Conditional}/>
                    <Route path="/user" component={User}/>
                    <Route path="/profiles" component={Profile}/>
                    <Route path="/" exact component={Home}/>
                    <Route component={Posts}/>

                </Switch>
            </div>
        </BrowserRouter>
    )
}

ReactDOM.render(
    <App/>, document.querySelector('#root'))