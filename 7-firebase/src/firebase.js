import * as firebase from 'firebase';
const config = {
    apiKey: "AIzaSyAx8GP5jGRKXX1qAPPiAcs_EDYn7QL1Pko",
    authDomain: "forms-test-603ba.firebaseapp.com",
    databaseURL: "https://forms-test-603ba.firebaseio.com",
    projectId: "forms-test-603ba",
    storageBucket: "forms-test-603ba.appspot.com",
    messagingSenderId: "671180885263"
  };

firebase.initializeApp(config);

const firebaseDB = firebase.database();

const googleAuth = new firebase.auth.GoogleAuthProvider();

export {
    firebase,
    firebaseDB,
    googleAuth,
}

// firebaseDB.ref('users').orderByChild('lastname').equalTo('Ball').once('value')
// .then((snapshot)=>[
//     console.log(snapshot.val())
// ])