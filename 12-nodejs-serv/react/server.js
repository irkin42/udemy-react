const express = require('express');
const app = express();

app.get('/api/users', (req,res)=>{
    res.json([
        {
            id:1,
            username: 'ksy'
        },
        {
            id:2,
            username: 'tom'
        }
    ])
})

app.get('/api/cars', (req,res)=>{
    res.json([
        {
            id:1,
            brand: 'ford'
        },
        {
            id:2,
            brand: 'nissan'
        }
    ])
})
const port = process.env.PORT || 3001;

app.listen(port)