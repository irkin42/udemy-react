import React from 'react';
import PropTypes from 'prop-types';

const UserTemplate = (props) => {
    console.log(props)
    return (
        <div>
            
        </div>
    );
};

UserTemplate.propTypes = {
    name: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.oneOf(['Frank', 'Irini'])
    ]),
    lastname: PropTypes.string,
    age: PropTypes.number,
    hobbies: PropTypes.arrayOf(PropTypes.string),
    spanish: PropTypes.bool,
    message: PropTypes.func,
    car: PropTypes.object,
    mother:function(props, propName, componentName){
        if(props[propName] !=='yes'){
            return new Error(`The name ${props[propName]} is incorrect`)
        }
    }

}
export default UserTemplate;