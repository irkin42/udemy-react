import React, { Component } from 'react';
import UserTemplate from './user_template';
class User extends Component {
    state = { 
        name: 'irini',
        lastname: 'nikolaidi',
        age: 28,
        hobbies: ['run', 'jump'],
        spanish: false,
        message(){console.log('hey')},
        car: {brand: 'ford', model: 'focus'},
        mother: 'yes'
    }
    render() {
        return (
            <div>
                <UserTemplate {...this.state}/>
            </div>
        );
    }
}

export default User;