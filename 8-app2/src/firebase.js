import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBosxHy2Gtw9ihAERXvdc4BAkreQAFJZpo",
    authDomain: "nba-full-d5ebc.firebaseapp.com",
    databaseURL: "https://nba-full-d5ebc.firebaseio.com",
    projectId: "nba-full-d5ebc",
    storageBucket: "nba-full-d5ebc.appspot.com",
    messagingSenderId: "647955837095"
  };

firebase.initializeApp(config);

const firebaseDB = firebase.database();
const firebaseArticles = firebaseDB.ref('articles');
const firebaseTeams = firebaseDB.ref('teams');
const firebaseVideos = firebaseDB.ref('videos');

const firebaseLooper = (snapshot) =>{
    const data = [];
    snapshot.forEach((snapshotChild) => {
        data.push({
            ...snapshotChild.val(),
            id: snapshotChild.key
        })
    });
    return data;

}

export {
    firebase,
    firebaseDB,
    firebaseArticles,
    firebaseTeams,
    firebaseVideos,
    firebaseLooper
}
