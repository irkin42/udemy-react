import React from 'react';
import VideosList from '../../../widgets/VideosList/VideosList';

const VideosMain = () => {
    return (
        <div>
            <VideosList
            start={0}
            amount={10}
            type='card'
            loadmore={true}
            />
        </div>
    );
};

export default VideosMain;