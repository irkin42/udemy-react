import React from 'react';
import FA from 'react-fontawesome';
import './cardInfo.css'

const CardInfo = (props) => {
    const teamName = (teams,team) => {
        let data = teams.find((item)=>{
            return item.id === team
        })
        if (data) {
            return data.name
        }
    }
    return(
    <div className="cardInfo">
    <span className="teamName">
    {teamName(props.teams,props.team)}
    </span>
    <span className="date"> 
        <FA
            name="clock-o"
        />
            {props.date}
    </span>
    </div>
    )
}

export default CardInfo;
