import React from 'react';
import VideosListTemplate from '../videosListTemplate';

const VideosRelated = (props) => ( // grab props and send them to another component. we use the same name and the same props.name
    <div>
        <div className="relatedWrapper">
            <VideosListTemplate
                data={props.data}
                teams={props.teams}
            />
        </div>
    </div>
);

export default VideosRelated;
