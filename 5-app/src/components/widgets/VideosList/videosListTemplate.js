import React from 'react';
import './videosList.css';

import {Link} from 'react-router-dom';
import CardInfo from '../CardInfo/CardInfo';

const VideosListTemplate = (props) => { // receive props from another component that use VideosListTemplate. for example VideosRelated uses this
    return props.data.map((item, i)=>{ // grab all props and use them as parts of information for element
        return <Link to={`/videos/${item.id}`} key={i}>
        <div className="videoListItem_wrapper">
            <div className="left"
                style={{
                    background: `url(/images/videos/${item.image})`
                }}
            >
                <div></div>
            </div>
            <div className="right">
            <CardInfo teams={props.teams} 
            team={item.team} date={item.date}/>
                <h2>{item.title}</h2>
            </div>
        </div>
        </Link>
    })
};

export default VideosListTemplate;