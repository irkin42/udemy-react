import React from 'react';

//1. receive props and convert formData to array formArray and render it by using renderTempleates
const FormFields = (props) => {
    const renderFields = () => {
        const formArray = [];
        for(let elementName in props.formData){
            formArray.push({
                id: elementName,
                settings: props.formData[elementName]
            })
        }

        return formArray.map((item,i)=>{
            return (
                <div key={i} className="form_element">
                    {renderTemplates(item)} 
                </div>
            )
        });
    }
    // 6. receive event and formArray[item].id and do some logic
    const changeHandler = (event,id,blur) => {
        // form newState by replacing formData
        const newState = props.formData; 
        // set value to event.target.value
        newState[id].value = event.target.value;
        // 6.1 use props function change with newState (new formData) go to user.js change
        if (blur){
            let validData = validate(newState[id]);
            newState[id].valid = validData[0];
            newState[id].validationMessage = validData[1];
            console.log(validData)
        }
        newState[id].touched = blur;
        props.change(newState)
    }

    const validate = (element) =>{
        console.log(element)
        let error = [true, ''];
        if(element.validation.minLen){
            const valid = element.value.length >= element.validation.minLen;
            const message = `${!valid ? 'Must be greater than ' + element. validation.minLen : '' }`
            error = !valid ? [valid,message] : error
        }
        if(element.validation.required){
            const valid = element.value.trim() !== '';
            const message = `${!valid ? 'this field is required' : ''}`;

            error = !valid ? [valid,message] : error
        }
        return error
    }
    const showLabel = (show, label) => {
        return show ? <label>{label}</label> : null
    }
    const showValidation = (data) =>{
        let errorMessage = null;
        if (data.validation && !data.valid){
            errorMessage= (
                <div className="label_error">
                    {data.validationMessage}
                </div>
            )
        }
        return errorMessage
    }
    // 2. receive formArray item and do some logic
    const renderTemplates = (data) =>{
        let formTemplate = '';
        let values = data.settings;
        // 3. if formArray[item].settings.element == input
        switch(values.element){
            case('input'):
            //4. lets form formtemplate
            formTemplate = (
                <div>
                   <div>{showLabel(values.label, values.labelText)} </div> 
                   <input 
                        {...values.config}
                        value={values.value}
                        onBlur={
                            (event) => changeHandler(event, data.id, true)
                        }
                        onChange={
                            // 5. on click get event and formArray[item].id and go to changeHadler
                            (event) => changeHandler(event, data.id, false)
                        }
                   />
                   {showValidation(values)}
                </div>               
            )
            break;
            case('textarea'):
            formTemplate = (
                <div>
                   <div>{showLabel(values.label, values.labelText)} </div> 
                   <textarea
                        {...values.config}
                        value={values.value}
                        onChange={
                            // 5. on click get event and formArray[item].id and go to changeHadler
                            (event) => changeHandler(event, data.id)
                        }
                    />
                </div>               
            )
            break;
            case('select'):
            formTemplate = (
                <div>
                   <div>{showLabel(values.label, values.labelText)} </div> 
                   <select
                        value={values.value}
                        name={values.config.name}
                        onChange={
                            // 5. on click get event and formArray[item].id and go to changeHadler
                            (event) => changeHandler(event, data.id)
                        }
                    >
                    {values.config.options.map((item,i)=>(
                        <option key={i} value={item.val}>
                            {item.text}
                        </option>
                    ))}
                    </select>
                </div>               
            )

            break;
            default:
            formTemplate = null;
        }
        return formTemplate;
    }
    return (
        <div>
            {renderFields()}
        </div>
    );
};

export default FormFields;